GET all
http://localhost:8080/v1/rest/students

GET students by ID
http://localhost:8080/v1/rest/students/1

ADD STUDENT
curl -H "Content-Type: application/json" -X POST -d "{\"id\": 10,\"firstName\":\"test\",\"lastName\":\"test\"}" http://localhost:8080/v1/rest/students

UPDATE
curl -H "Content-Type: application/json" -X PUT -d "{\"id\": 10,\"firstName\":\"test\",\"lastName\":\"test\"}" http://localhost:8080/v1/rest/students


DELETE
curl -X DELETE http://localhost:8080/v1/rest/students/3
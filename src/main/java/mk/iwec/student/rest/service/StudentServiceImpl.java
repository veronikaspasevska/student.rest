package mk.iwec.student.rest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.iwec.student.rest.repository.*;

import mk.iwec.student.rest.domain.Student;
import mk.iwec.student.rest.exception.StudentNotFoundException;

@Service
public class StudentServiceImpl implements Services<Integer, Student> {
	@Autowired
	private StudentRepository studentRepository;

	@Override
	public List<Student> getAll() {
		return studentRepository.findAll();

	}

	@Override
	public Student getById(Integer id) {
		Optional<Student> student = studentRepository.findById(id);
		if (student.isPresent()) {
			return student.get();

		} else {
			throw new StudentNotFoundException("Student with id " + id + " is not present");
		}
	}

	@Override
	public Student add(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public void update(Student student) {
		if (studentRepository.findById(student.getId()).isPresent()) {
			studentRepository.save(student);
		} else {
			throw new StudentNotFoundException("Student with id " + student.getId() + " is not present");
		}
	}

	@Override
	public void delete(Integer id) {
		if (studentRepository.findById(id).isPresent()) {
			studentRepository.deleteById(id);
		} else {
			throw new StudentNotFoundException("Student with id " + id + " is not present");

		}
	}

}

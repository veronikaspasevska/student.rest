package mk.iwec.student.rest.service;

import java.util.List;

import org.springframework.stereotype.Service;


@Service
public interface Services<K, E> {

	public List<E> getAll();

	public E getById(K key);

	public E add(E entity);

	public void update(E entity);

	public void delete(K key);

}

package mk.iwec.student.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.student.rest.domain.Student;
import mk.iwec.student.rest.service.StudentServiceImpl;
import mk.iwec.student.rest.util.*;

@RestController
@RequestMapping("v1/rest/students")
public class StudentController implements Controller<Integer, Student> {

	@Autowired
	private StudentServiceImpl studentServiceImpl;

	@RequestMapping(method = RequestMethod.GET, produces = Constants.PRODUCES)
	@ResponseBody
	public List<Student> getAll() {
		return studentServiceImpl.getAll();
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Student> getById(@PathVariable(value = "id") Integer id) {
		Student student = studentServiceImpl.getById(id);
		HttpStatus status = student != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
		return new ResponseEntity<Student>(student, status);
	}

	@PostMapping(consumes = Constants.CONSUMES)
	@ResponseStatus(HttpStatus.CREATED)
	public void insert(@RequestBody Student student) {
		studentServiceImpl.add(student);
	}

	@PutMapping(consumes = Constants.CONSUMES)
	@ResponseStatus(HttpStatus.OK)
	public void update(@RequestBody Student student) {
		studentServiceImpl.update(student);
	}

	@DeleteMapping(value = "/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable(value = "id") Integer id) {
		studentServiceImpl.delete(id);

	}

}

package mk.iwec.student.rest.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

public interface Controller<K, E> {

	public List<E> getAll();

	public ResponseEntity<E> getById(K key);

	public void insert(E entity);

	public void update(E entity);

	public void delete(K key);

}
